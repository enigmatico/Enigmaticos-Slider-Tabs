var sliders = new Array();

function PanelSlider(panelid, params={anim: true, animation_time: 1000}){
    this.root = null;
    this.slides = null;
    this.container = null;
    this.panelsCarousel;
    this.totalwidth = 0;
    this.slideWidth = 0;
    this.slideHeight = 0;
    this.selectedSlide = 0;
    this.animate = params.anim;
    this.anime_time = params.animation_time;

    this.root = $("#" + panelid);
    this.container = $(this.root.find('.panels-container')[0]);
    this.slides = this.container.find('div');
    this.panelsCarousel = $(this.root.find(".panels-carousel"));

    sliders.push(this);
    this.updateSlides = function(){

        this.slideWidth = parseInt(this.root.width())+1;
        this.slideHeight = parseInt(this.root.height())+1;
        this.totalWidth = this.slideWidth * Object.keys(this.slides).length;
    
        this.container.css('width', this.totalWidth+'px');
        for(x = 0; x < Object.keys(this.slides).length; x++)
            $(this.slides[x]).css('width',this.slideWidth+'px').css('height',this.slideHeight+'px');
    
        this.panelsCarousel.css('margin-left', '-'+(this.panelsCarousel.width()/2)+'px');
        this.container.css('left', '-'+(this.selectedSlide*this.slideWidth) + 'px');
    }
    
    this.initializeSlider = function(){
        var obj = this;
        obj.updateSlides();
        obj.panelsCarousel.find("button").each(function(){
            $(this).on('click',function(){
                $(obj.panelsCarousel.find("button.selected")).removeClass('selected');
                obj.selectedSlide = parseInt($(this).attr("rel")-1);

                var rSlide = parseInt($(this).attr("rel")-1);
                var rLeft = -(rSlide * obj.slideWidth);
                var rDiff = obj.container.position().left - rLeft;

                if(obj.animate)
                {
                    obj.container.animate({
                        left: "-=" +rDiff,
                    },obj.anime_time,function(){
                        obj.container.css('left', '-'+(obj.selectedSlide*obj.slideWidth) + 'px');
                    });
                }else{
                    obj.container.css('left', '-'+(obj.selectedSlide*obj.slideWidth) + 'px');
                }


                $(this).addClass('selected');
            });
        }); 
    }

    $(window).resize(function(){
        $.each(sliders,function(index,slideri){
            slideri.updateSlides();
        });
    });
}