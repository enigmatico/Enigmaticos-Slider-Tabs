# Enigmatico's Sliders Tabs
### Modular JavaScript sliders/tabs views for your website!

**- Features**:

 - Fully customizable slider frames
 - Sliding animations (Can be modified/turned off)
 - Full responsive design
 - Very easy to set up!

**- Requirements**

 - jQuery (Recommended version 3.3.1)

**- Setting up**

Wherever you want a slider to appear, add the following HTML code:

    <div  class="container-class">
	    <div  class="module-panels"  id="panels-id">
		    <div  class="panels-container">
		    <!-- Your panels go here -->
		    </div>
    
		    <div  class="panels-carousel">
		    <!-- Your buttons go here -->
		    </div>
    
	    </div>
    </div>

Change ***container-class*** for whatever CSS class name you want for the container, and make sure in your CSS code that at least it's X overflow is set to ***none***.

    .container-class{
	    overflow-x: none;
    }

Change ***panels-id*** for whatever ID you want for your slider. Just make sure that it is unique. You can add as many sliders as you want.

Everything else must be like in the given code.

**- Adding Panels**

The panels are just DIVs with class *panel* and a rel attribute indicating their order. The panels go inside the panels-container DIV. They are defined like this:

    <div  class="panel"  rel="1"><!-- Your content --></div>
Change the rel value for the index of the panel, in the same order you add them (from 1 to *n*).

**- Adding buttons**

The buttons go inside the *panels-carousel* DIV. Just add as many buttons as frames in the panels-container. Just like the panels, the buttons have a rel attribute with their index in the same order you add them. 

    <button class="selected" rel="1"><!-- Button content --></button>

However, unlike the panels, these don't have any predefined class except for the current active button, which will have a *selected* class while active.

You can add a 'selected' class to the first button if you want it selected, but make sure there is only one selected button.

**- Adding the JavaScript**

Make sure to include sliders.js or sliders.min.js in your HTML, as well as sliders.css or sliders.min.css. Also make sure you have jQuery included ***BEFORE*** the sliders.

    <script  language="javascript"  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script  language="javascript"  src="js/sliders.min.js"></script>
    <link  rel="stylesheet"  type="text/css"  href="css/sliders.min.css">

After the DOM has been loaded, add the following JS code:

    var  slider  =  new PanelSlider("panels-id",{anim: true, animation_time: 900});
    slider.initializeSlider();
This will initialize your panel. For every panel in your page, add a new instance of PanelSlider and initialize it with its initializeSlider function.

The constructor of PanelSlider takes two parameters. First, the ID of the panel. And second, a structure containing the options.

The possible options are:

| option | explanation |
|--|--|
|animation  | Enables or disables the animation (Boolean) |
|animation_time  | How many milliseconds does the animation lasts (Integer) |

